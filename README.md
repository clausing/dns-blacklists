https://bitbucket.org/ethanr/dns-blacklists/

It's a python script so you'll need a machine with python installed.  You need 2 other things to run this script: 

1. A directory with files containing IPs/domains that you want to detect.  There is a bad_lists directory included that has a bunch of lists I pulled from various sources on the web.  
2. A directory with files containing the IPs/domains that your systems are communicating with.  You can use DNS logs or any traffic logs you keep that have the remote IP/domains listed.

You run it like this:

`python dns-blacklists.py bad_lists_directory your_traffic_directory`

It uses regular expressions to pull out IP addresses and hostnames so the files don't need to be formatted in any particular way.  It will then show any that it finds in common between the two directories, so in this use case it will tell you which sites in bad_lists_directory that your systems are communicating with.  Some caveats are:

- It does not do DNS or reverse DNS resolution.  So if bad_site.com resolves to 5.5.5.5 and you have bad_site.com in the bad_lists but are seeing communication to 5.5.5.5 it will not detect this.  I may add this feature in the future, but it will take much longer and will pollute the local DNS records unless I am very careful.
- The detection is only as good as the bad lists you provide.  I grabbed a bunch from the web to have as defaults, but to be more effective you will have to tune them or replace them with your own.  For instance, if you are being attacked or want to know if you are being attacked from a particular country you could provide a list of IP addresses from that geographic location.
- It only handles single IPs or hostnames.  So you can't specify blocks of IPs using CIDR notation or anything else.  This seems like a good feature to add so this may be next on my list.